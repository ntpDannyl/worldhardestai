import Enemy from "./Enemy.js";

export default class Levels {
    static DEFAULT_LEVELS = [
        {
            name: 'Peaceful',
            player: {
                x: 30,
                y: 170
            },
            enemies: [],
            walls:[],
            goals: [
                {
                    x: 490,
                    y: 0,
                    w: 150,
                    h: 360,
                },
            ]
        },
        {
            name: 'Noob',
            player: {
                x: 30,
                y: 170
            },
            enemies: [],
            walls:[
                {
                    x: 250,
                    y: 0,
                    w: 30,
                    h: 300,
                }
            ],
            goals: [
                {
                    x: 490,
                    y: 0,
                    w: 150,
                    h: 360,
                },
            ]
        },
        {
            name: 'Easy',
            player: {
                x: 30,
                y: 170
            },
            enemies: [],
            walls:[
                {
                    x: 150,
                    y: 30,
                    w: 30,
                    h: 220,
                },
                {
                    x: 350,
                    y: 170,
                    w: 30,
                    h: 150,
                },
                {
                    x: 290,
                    y: 290,
                    w: 60,
                    h: 30,
                },
            ],
            goals: [
                {
                    x: 490,
                    y: 180,
                    w: 150,
                    h: 180,
                },
            ]
        },
        {
            name: 'Middle',
            player: {
                x: 30,
                y: 170
            },
            enemies: [],
            walls:[
                {
                    x: 150,
                    y: 30,
                    w: 30,
                    h: 120,
                },
                {
                    x: 200,
                    y: 210,
                    w: 30,
                    h: 120,
                },
                {
                    x: 250,
                    y: 30,
                    w: 30,
                    h: 120,
                },
                {
                    x: 300,
                    y: 210,
                    w: 30,
                    h: 120,
                },
                {
                    x: 350,
                    y: 30,
                    w: 30,
                    h: 120,
                },
                {
                    x: 400,
                    y: 210,
                    w: 30,
                    h: 120,
                },
            ],
            goals: [
                {
                    x: 540,
                    y: 120,
                    w: 100,
                    h: 120,
                },
            ]
        },
        {
            name: 'Hard',
            player: {
                x: 30,
                y: 170
            },
            enemies: [
                {
                    x: 150,
                    y: 30,
                    mode: Enemy.MODE_VERTICAL,
                    direction: Enemy.FORWARD,
                },
                {
                    x: 270,
                    y: 230,
                    mode: Enemy.MODE_HORIZONTAL,
                    direction: Enemy.BACKWARD,
                },
            ],
            walls:[
                {
                    x: 150,
                    y: 210,
                    w: 30,
                    h: 120,
                },

            ],
            goals: [
                {
                    x: 490,
                    y: 0,
                    w: 150,
                    h: 360,
                },
            ]
        },
        {
            name: 'Nightmare',
            player: {
                x: 270,
                y: 170
            },
            enemies: [
                {
                    x: 30,
                    y: 30,
                    mode: Enemy.MODE_VERTICAL,
                    direction: Enemy.FORWARD,
                },
                {
                    x: 30,
                    y: 330,
                    mode: Enemy.MODE_VERTICAL,
                    direction: Enemy.BACKWARD,
                },
                {
                    x: 450,
                    y: 30,
                    mode: Enemy.MODE_VERTICAL,
                    direction: Enemy.FORWARD,
                },
                {
                    x: 450,
                    y: 330,
                    mode: Enemy.MODE_VERTICAL,
                    direction: Enemy.BACKWARD,
                },
                {
                    x: 540,
                    y: 150,
                    mode: Enemy.MODE_HORIZONTAL,
                    direction: Enemy.BACKWARD,
                },
                {
                    x: 100,
                    y: 150,
                    mode: Enemy.MODE_HORIZONTAL,
                    direction: Enemy.FORWARD,
                },
                {
                    x: 380,
                    y: 60,
                    mode: Enemy.MODE_VERTICAL,
                    direction: Enemy.FORWARD,
                },
            ],
            walls:[
                {
                    x: 390,
                    y: 60,
                    w: 10,
                    h: 80,
                },
                {
                    x: 510,
                    y: 120,
                    w: 10,
                    h: 30,
                },
                {
                    x: 510,
                    y: 140,
                    w: 30,
                    h: 10,
                },
            ],
            goals: [
                {
                    x: 520,
                    y: 120,
                    w: 20,
                    h: 20,
                },
                {
                    x: 0,
                    y: 120,
                    w: 20,
                    h: 20,
                },
                {
                    x: 420,
                    y: 60,
                    w: 20,
                    h: 20,
                },
                {
                    x: 620,
                    y: 340,
                    w: 20,
                    h: 20,
                },
            ]
        },
    ]
}