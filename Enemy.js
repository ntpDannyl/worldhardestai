import Map from "./Map.js";

function getRndInteger(min, max) { //max is excluded
    return Math.floor(Math.random() * (max - min) ) + min;
}

export default class Enemy{
    static MODE_HORIZONTAL = 0
    static MODE_VERTICAL = 1
    static MODE_RANDOM = 2
    static STEPRANGE = 10
    static FORWARD = 1
    static BACKWARD = -1
    constructor(map, startX, startY, mode, direction) {
        this.map = map
        this.x = startX
        this.y = startY
        this.startX = startX
        this.startY = startY
        this.mode = mode
        this.startDirection = direction
        this.direction = direction
    }

    move(){
        switch(this.mode) {
            case Enemy.MODE_HORIZONTAL:
                // Schaue ob Wand oder Ziel oder OOB: eventuell umdrehen
                if (this.x+Enemy.STEPRANGE*this.direction < 0 || this.x+Enemy.STEPRANGE*this.direction > this.map.width - Enemy.STEPRANGE){
                    this.direction *= -1
                } else {
                    const inFrontOfMe = this.map.checkPoint(this.x+Enemy.STEPRANGE*this.direction,this.y)
                    if(inFrontOfMe === Map.WALL || inFrontOfMe === Map.GOAL){
                        this.direction *= -1
                    }
                }
                this.x += Enemy.STEPRANGE * this.direction  // bewegen
                break
            case Enemy.MODE_VERTICAL:
                // Schaue ob Wand oder Ziel oder OOB: eventuell umdrehen
                if (this.y+Enemy.STEPRANGE*this.direction < 0 || this.y+Enemy.STEPRANGE*this.direction > this.map.height - Enemy.STEPRANGE){
                    this.direction *= -1
                } else {
                    const inFrontOfMe = this.map.checkPoint(this.x,this.y+Enemy.STEPRANGE*this.direction)
                    if(inFrontOfMe === Map.WALL || inFrontOfMe === Map.GOAL){
                        this.direction *= -1
                    }
                }
                this.y += Enemy.STEPRANGE * this.direction  // bewegen
                break
            case Enemy.MODE_RANDOM:
                getRndInteger(0,8)
                // Problem, man kann nicht immer zufällig sein, weil das Bewegungsmuster des Gegners sich nicht
                // von Generation zu Generation veränderm darf.

                // würfle zufällige Bewegung
                // schaue ob Wand oder Ziel oder OOB in zufälliger Richtung
                // wenn ja, rekursives Aufrufen von move()
                // wenn nein, bewegen
                console.log('This mode will be implemented soonTM')
                break
            default:
                console.error("Critical error in Enemy.move()")
        }
    }

    reset(){
        this.x = this.startX
        this.y = this.startY
        this.direction = this.startDirection
    }
}