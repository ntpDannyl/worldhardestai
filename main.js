import Population from './Population.js'
import Levels from "./Levels.js"
import Cookie from "./Cookie.js"

window.$ = $;
// @TODO: Slider für die Zeit/FPS
// @TODO: Enemy-Mode: Patrol
// @TODO: Startpunkt verschieben

let levels
let canvas = document.getElementById('myCanvas')
let ctx = canvas.getContext('2d')
let gameInterval
let level = 0
let gens = []
let fitnesses = []
let $input = $('#import')
let $levelSelect = $('#levelSelect')
init(true)

$('#resetButton').on('click', ()  => {
    init(false);
})

$levelSelect.on('change', () =>{
    level = Number($levelSelect.val())
    init(false)
})

$('#exportButton').on('click', ()  => {
    const a = document.createElement("a")
    // we are creating a list with one level as content in order to streamline our import
    const file = new Blob([JSON.stringify([levels[level]])], {type: 'application/json'})
    a.href = URL.createObjectURL(file)
    a.download = `Level ${levels[level].name}`
    a.click()
})

$('#exportAllButton').on('click', ()  => {
    const a = document.createElement("a")
    const file = new Blob([JSON.stringify(levels)], {type: 'application/json'})
    a.href = URL.createObjectURL(file)
    a.download = 'All Levels'
    a.click()
})

$input.on('change', ()  => {
    const reader = new FileReader()
    reader.readAsText($input[0].files[0], "UTF-8")
    reader.onload = (event) => {
        const newLevels = JSON.parse(event.target.result)
        for (const newLevel of newLevels){
            let alreadyExists = false
            for(const level of levels){
                if(newLevel.name === level.name) {
                    alreadyExists = true
                    break
                }
            }
            if(!alreadyExists){
                levels.push(newLevel)
                console.log(`Level ${newLevel.name} hinzugefügt.`)
            } else {
                console.log(`Level ${newLevel.name} existiert bereits.`)
            }
        }
        Cookie.set("Levels", levels)
        init(true)
    }
})

$('#playButton').on('click', () => {
    const pop = new Population(50, levels[level])

    gameInterval = setInterval(() => {
        if(pop.allPlayersDead()){
            pop.calculateFitness()
            pop.naturalSelection()
            pop.mutateBabies()
            if(pop.gen % 5 === 0){
                pop.increaseMoves(25)
            }
            pop.resetEnemies()
            // startscreen animieren
            updateInfos(pop.goalFound, pop.gen, pop.bestPlayer, pop.bestPlayerFitness, pop.maxStep)
            render(pop.players, pop.enemies)
        } else {
            pop.update()
            render(pop.players, pop.enemies)
        }
    }, 1000/20 /*fps*/)
})

function updateInfos(bGoalFound, gen, index, fitness, steps){
    if (bGoalFound) {
        $("#goalFound").text("We found the goal, now we optimizing our way to get there.")
    }
    $("#generation").text(gen)
    $("#bestPlayerNumber").text(index)
    $("#bestPlayerFitness").text(fitness)
    $("#bestPlayerSteps").text(steps)
    gens.push(gen)
    fitnesses.push(fitness)
    Plotly.redraw('plot')
}

function initPlot(){
    Plotly.newPlot('plot', [{
        x: gens,
        y: fitnesses,
        type:'scatter',
        mode: 'lines',
        line: {
            color: '#62a287',
            width: 4
        },
    }], {
        paper_bgcolor: "rgba(0,0,0,0)",
        plot_bgcolor: "rgba(0,0,0,0)",
        xaxis: {
            gridcolor: 'rgba(255, 255, 255,0.25)',
            zerolinecolor: 'rgba(255, 255, 255,0.5)',
            zerolinewidth: 2,
            tickfont: {
                color: 'rgba(255, 255, 255,0.5)'
            }
        },
        yaxis: {
            gridcolor: 'rgba(255, 255, 255, 0.25)',
            zerolinecolor: 'rgba(255, 255, 255,0.5)',
            zerolinewidth: 2,
            tickangle: 45,
            exponentformat: 'e',
            tickfont: {
                color: 'rgba(255, 255, 255,0.5)'
            }
        },
    });
}

function init(updateSelect){
    gens = []
    fitnesses = []
    levels = Cookie.get("Levels")
    if(!levels){
        console.log("No saved levels found, creating a list with default ones.")
        levels = Levels.DEFAULT_LEVELS
        Cookie.set("Levels", levels)
    }
    if(updateSelect){
        updateLevelSelect()
    }
    level = Number($levelSelect.val())
    initPlot()
    updateInfos(false, 0, 0, 0, 0)
    $("#goalFound").text("We didnt find the goal yet. Please let us learn first.")
    render([levels[level].player], levels[level].enemies)
    clearInterval(gameInterval)
}

function updateLevelSelect(){
    let innerHTML = ''
    for(let i in levels){
        if(!levels.hasOwnProperty(i)){
            continue
        }
        innerHTML += `<option value="${i}">${levels[i].name}</option>`
    }
    $levelSelect[0].innerHTML = innerHTML;
}

function render(players, enemies){
    drawLevel()
    if(enemies){
        for(const enemy of enemies){
            createRect(enemy.x+1, enemy.y+1,8, 8, 'blue', 'black', 2) // Enemies
        }
    }
    createRect(players[0].x+1, players[0].y+1,8, 8, 'red', 'black', 2) // Best Player
    for(let i = 1; i < players.length; i++) { // all other Players
        createEcplipse(players[i].x + 5, players[i].y + 5)
    }
}

function drawLevel(){
    clearAll()
    drawBackground()
    const lvl = levels[level]
    for(const goal of lvl.goals) {
        createRect(goal.x, goal.y, goal.w, goal.h, 'lightgreen') // Goal
    }
    for(const wall of lvl.walls){
        createRect(wall.x,wall.y, wall.w, wall.h, 'black') // Obstacle
    }
}

function drawBackground(){
    let white = true
    for(let i=0; i<canvas.width; i+=10){
        if(canvas.height % 20 === 0) {
            white = !white
        }
        for(let j=0; j<canvas.height; j+=10){
            if(white) {
                createRect(i, j, 10, 10, "white")
            } else {
                createRect(i, j, 10, 10, "#D6D3D4")
            }
            white = !white
        }
    }
}

function createEcplipse(x, y){
    ctx.beginPath()
    ctx.arc(x, y, 1, 0, 2 * Math.PI, false);
    stroke('black', 1)
    ctx.closePath()
}

function createRect(x, y, w, h, colorFill, colorStroke, strokeWidth){
    ctx.beginPath()
    ctx.rect(x, y, w, h)
    fill(colorFill)
    if(colorStroke && strokeWidth){
        stroke(colorStroke, strokeWidth)
    }
    ctx.closePath()
}

function fill(color) {
    ctx.fillStyle = color
    ctx.fill()
}

function stroke(color, width) {
    ctx.strokeStyle = color
    ctx.lineWidth = width
    ctx.stroke()
}

function clearAll(){
    ctx.clearRect(0, 0, canvas.width, canvas.height)
}

