const X = 1
const Y = 0

export default class Map {
    static NOTHING = 0
    static WALL = 1
    static GOAL = 2
    constructor(level) {
        this.goalPoints = []
        this.height = 360
        this.width = 640
        this.plan = new Array(this.height)
        for(let i = 0; i < this.height; i++){
            this.plan[i] = new Array(this.width).fill(Map.NOTHING);
        }
        for(const goal of level.goals) {
            this.create(Map.GOAL, goal.x, goal.y, goal.w, goal.h)
        }
        for(const wall of level.walls){
            this.create(Map.WALL, wall.x, wall.y, wall.w, wall.h)
        }
    }

    create(TYPE, x, y, w, h){
        for(let row = y; row < y + h; row++){
            for(let pixel = x; pixel < x + w; pixel++){
                this.plan[row][pixel] = TYPE;
                if(TYPE === Map.GOAL) {
                    this.goalPoints.push([row, pixel])
                }
            }
        }
    }
    
    checkPoint(x, y){
        return this.plan[y][x]
    }

    getClostestDistance(x, y){
        const distances = []
        for(let i = 0; i < this.goalPoints.length; i++){
            distances.push(Math.sqrt((this.goalPoints[i][X]-x)**2 + (this.goalPoints[i][Y]-y)**2))
        }
        return Math.min(...distances)
    }
}