import Player from "./Player.js";
import Map from "./Map.js"
import Enemy from "./Enemy.js";

export default class Population {
    constructor(size, level) {
        this.map = new Map(level)
        this.fitnessSum = 0.0
        this.gen = 0
        this.bestPlayer = 0
        this.bestPlayerFitness = 0
        this.maxStep = 2000
        this.players = []
        this.enemies = []
        this.goalFound = false
        for(let i =0; i < size; i++){
            this.players.push(new Player(this.map, level.player.x, level.player.y))
        }
        if(level.enemies){
            for(const enemy of level.enemies){
                this.enemies.push(new Enemy(this.map, enemy.x, enemy.y, enemy.mode, enemy.direction))
            }
        }
    }

    moveEnemies(){
        if(this.enemies){
            for(const enemy of this.enemies){
                enemy.move()
            }
        }
    }

    resetEnemies(){
        if(this.enemies){
            for(const enemy of this.enemies){
                enemy.reset()
            }
        }
    }

    update(){ // update all players
        this.moveEnemies()
        for(let i =0; i < this.players.length; i++) {
            if(this.players[i].brain.step > this.maxStep){
                this.players[i].dead = true
            } else {
                this.players[i].update(this.enemies)
            }
        }
    }

    calculateFitness(){
        for(let i =0; i < this.players.length; i++) {
            this.players[i].calculateFitness()
        }
    }

    calculateFitnessSum(){
        this.fitnessSum = 0
        for(let i =0; i < this.players.length; i++) {
            this.fitnessSum += this.players[i].fitness
        }
    }

    allPlayersDead(){  // returns whether all players are either dead or reached the goal
        for(let i = 0; i < this.players.length; i++) {
            if(!this.players[i].dead && !this.players[i].reachedGoal){
                return false
            }
        }
        return true
    }

    selectParent(){  // -> documentation page 32
        const randomFloat = getRandomFloatInclusive(0, this.fitnessSum)
        let runningSum = 0.0
        for(let i =0; i < this.players.length; i++) {
            runningSum += this.players[i].fitness
            if(runningSum > randomFloat){
                return this.players[i]
            }
        }
        console.log('Critical Error in population.selectParent()')
    }

    naturalSelection(){  // get the next gen of players
        let newPlayers = []
        this.setBestPlayer()
        this.calculateFitnessSum()

        newPlayers.push(this.players[this.bestPlayer].getBaby())
        newPlayers[0].isBest = true
        for (let i = 1; i < this.players.length; i++) {
            newPlayers.push(this.selectParent().getBaby())
        }
        this.players = newPlayers
        this.gen++
    }

    mutateBabies() {
        for (let i = 1; i < this.players.length; i++) { // we don't want to mutate the best player
            this.players[i].brain.mutate()
        }
    }

    setBestPlayer(){
        let maxFitness = 0.0
        let maxIndex = 0
        for (let i = 0; i < this.players.length; i++) {
            if(this.players[i].fitness > maxFitness){
                maxFitness = this.players[i].fitness
                maxIndex = i
            }
        }
        this.bestPlayerFitness = maxFitness
        this.bestPlayer = maxIndex
        if(this.players[this.bestPlayer].reachedGoal){
            this.maxStep = this.players[this.bestPlayer].brain.step
            this.goalFound = true
        }
    }

    increaseMoves(size){
        if(this.players[0].brain.directions.length < this.maxStep && !this.goalFound){
            for (let i = 0; i < this.players.length; i++) {
                this.players[i].brain.increase_moves(size)
            }
        }
    }
}

function getRandomFloatInclusive(min, max) {
    return Math.random() * (max - min) + min; //The maximum is inclusive and the minimum is inclusive
}