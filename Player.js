import Brain from './Brain.js'

// Directions
import Map from "./Map.js";
const UP = 0
const UPRIGHT = 1
const RIGHT = 2
const DOWNRIGHT = 3
const DOWN = 4
const DOWNLEFT = 5
const LEFT = 6
const UPLEFT = 7


const STEPRANGE = 10

export default class Player{
    constructor(map, startX, startY) {
        this.brain = new Brain(200)
        this.dead = false
        this.reachedGoal = false
        this.isBest = false
        this.fitness = 0.0
        this.map = map
        this.startX = startX
        this.startY = startY
        this.x = startX
        this.y = startY
    }

    move(){
        if(this.brain.step >= this.brain.directions.length){ // Keine Spielzüge übrig
            this.dead = true
            return
        }
        switch (this.brain.directions[this.brain.step]){
            case UP:
                this.y -= STEPRANGE
                this.brain.energyUsed += 1
                break
            case UPRIGHT:
                this.y -= STEPRANGE
                this.x += STEPRANGE
                this.brain.energyUsed += 2
                break
            case RIGHT:
                this.x += STEPRANGE
                this.brain.energyUsed += 1
                break
            case DOWNRIGHT:
                this.y += STEPRANGE
                this.x += STEPRANGE
                this.brain.energyUsed += 2
                break
            case DOWN:
                this.y += STEPRANGE
                this.brain.energyUsed += 1
                break
            case DOWNLEFT:
                this.y += STEPRANGE
                this.x -= STEPRANGE
                this.brain.energyUsed += 2
                break
            case LEFT:
                this.x -= STEPRANGE
                this.brain.energyUsed += 1
                break
            case UPLEFT:
                this.y -= STEPRANGE
                this.x -= STEPRANGE
                this.brain.energyUsed += 2
                break
            default:
                console.log('Something went horribly wrong in Player.move()')
        }
        this.brain.step += 1
    }

    update(enemies){ // move and collision detection
        if(this.dead || this.reachedGoal){
            return
        }
        this.move()
        if(this.x < 0 || this.y < 0 || this.x > this.map.width - 1 || this.y > this.map.height - 1){
            // wir müssen bei beiden -1 rechnen, da die Breite der Canvas 640 ist, der Array geht aber nur von 0 bis 639
            this.dead = true // kill when out of bounds
            return
        }
        if(this.map.checkPoint(this.x, this.y) === Map.WALL){
            this.dead = true
            return
        }
        for(const enemy of enemies){
            if(enemy.x === this.x && enemy.y === this.y){
                this.dead = true
                return
            }
        }
        if(this.map.checkPoint(this.x, this.y) === Map.GOAL){
            this.reachedGoal = true
        }
    }

    calculateFitness() {
        if(this.reachedGoal){
            this.fitness = 1 + 10000/(this.brain.energyUsed**2)
        } else {
            this.fitness = 1 / (this.map.getClostestDistance(this.x, this.y)**2)
        }
    }

    getBaby() {
        const baby = new Player(this.map, this.startX, this.startY)
        baby.brain = this.brain.clone()
        return baby
    }
}