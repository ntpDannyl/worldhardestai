# World's hardest AI

This is a little project to show how an evolutionary algorithm works.
There are different levels to select. You can start the game and watch the AI learn.

![Preview 1](./preview1.png "Preview 1")
![Preview 2](./preview2.png "Preview 2")


Features:
- Different players that learn via evolution. You see the best player as a red square and the other ones as little dots.
- Goals to reach (Multiple goals work)
- Walls to die
- Ememies to also die
- Saving levels via cookies, importing and exporting them, you can create levels in JSON.
- Graphs

Todo:
- An actual level editor
- Multiple design changes
- A "How it works" screen

## Inspired by
[CodeBullet](https://www.youtube.com/watch?v=Yo2SepcNyw4&ab_channel=CodeBullet)


## Free to use
You can use the code freely of course.
