function getRndInteger(min, max) { //max is excluded
    return Math.floor(Math.random() * (max - min) ) + min;
}

export default class Brain {
    constructor(size) {
        this.directions = []
        this.step = 0
        this.energyUsed = 0
        this.increase_moves(size)
    }

    increase_moves(size){
        for(let i = 0; i < size; i++){
            this.directions.push(getRndInteger(0,8))
        }
    }

    clone(){
        const clone = new Brain(0)
        for(let i=0; i < this.directions.length; i++){
            clone.directions.push(this.directions[i])
        }
        return clone
    }

    mutate(){
        for(let i=0; i < this.directions.length; i++){
            if(Math.random() < 0.05){ // in ~10% der Fälle
                this.directions[i] = getRndInteger(0,8)
            }
        }
    }
}